<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function formRegister(){
        return view('register');
    }
    public function inputRegister(Request $req){
        $firstname =  $req['Fname'];
        $lastname = $req['Lname'];
        return view('welcome',compact('firstname','lastname'));
    }
}
