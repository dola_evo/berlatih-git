<?php
require 'animal.php';
require 'ape.php';
require 'frog.php';

// Release 1
$sheep = new Animal("shaun");
echo "Name : ".$sheep->name."<br>"; // "shaun"
echo "legs : ".$sheep->legs."<br>"; // 4
echo "cold_blooded : ".$sheep->cold_blooded."<br><br>"; // "no"

// Release 2
$kodok = new Frog("buduk");
echo "Name : ".$kodok->name."<br>"; // "shaun"
echo "legs : ".$kodok->legs."<br>"; // 4
echo "cold_blooded : ".$kodok->cold_blooded."<br>"; // "no"
$kodok->jump() ; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "Name : ".$sungokong->name."<br>"; // "shaun"
echo "legs : ".$sungokong->legs."<br>"; // 4
echo "cold_blooded : ".$sungokong->cold_blooded."<br>"; // "no"
$sungokong->yell(); // "Auooo"
?>