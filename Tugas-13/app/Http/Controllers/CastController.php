<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        // dd($cast);
        return view('cast/index', compact('cast'));
    }
    public function create(){
        return view('cast/create');
    }
    public function store(Request $request){
        // return view('')
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required|min:6'
        ],
        [
            'nama.required' =>  'nama tidak boleh kosong',
            'umur.required' => 'umur tidak boleh kosong',
            'umur.numeric' => 'umur harus berupa angka',
            'bio.required' => 'bio tidak boleh kosong',
            'bio.min' => 'bio minimal 6 karakter'
        ]);
        DB::table('cast')->insert(
            ['nama' => $request['nama'],
             'umur' => $request['umur'],
             'bio' => $request['bio']
            ]
        );
        return redirect('/cast');
    }
    public function show ($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.detail',compact('cast'));
    }
    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.update',compact('cast'));
    }
    public function update(Request $request,$id){
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required|min:6'
        ],
        [
            'nama.required' =>  'nama tidak boleh kosong',
            'umur.required' => 'umur tidak boleh kosong',
            'umur.numeric' => 'umur harus berupa angka',
            'bio.required' => 'bio tidak boleh kosong',
            'bio.min' => 'bio minimal 6 karakter'
        ]);
        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'] ,
                'umur' => $request['umur'],
                'bio' =>$request['bio']
            ]);

        return redirect ('/cast');
    }
    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        return redirect ('/cast');
    }
}
