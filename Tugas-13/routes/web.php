<?php



Route::get('/', function(){
    return view("layouts/master");
});
Route::get('/table',function(){
    return view ("layouts/table");
});
Route::get('/data-tables',function(){
    return view ("layouts/dataTable");
});

// CRUD CAST

//CREATE
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');

//READ
Route::get('/cast','CastController@index');
Route::get('/cast/{cast_id}','CastController@show');

//UPDATE
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');

//DELETE
Route::delete('/cast/{cast_id}', 'CastController@destroy');