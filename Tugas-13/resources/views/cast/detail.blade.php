@extends('layouts.master')
@section('title')
    Form isi data cast
@endsection

@section('content')
    <h1 class="text-primary">{{ $cast -> nama }}</h1>
    <p>Umur : {{ $cast -> umur }}</p>
    <p>Bio : {{ $cast -> bio }}</p>

    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection