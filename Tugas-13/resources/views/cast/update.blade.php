@extends('layouts.master')
@section('title')
    hamalan update cast
@endsection

@section('content')
    

    <form action="/cast/{{ $cast -> id }}" method="POST">
        @csrf
        @method('put')
      <div class="form-group">
        <label for="exampleInputEmail1">Nama</label>
        <input type="text" class="form-control" name="nama" value="{{ old('nama', $cast -> nama) }}">
      @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
       
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Umur</label>
        <input type="text" class="form-control" name="umur" value="{{ old('umur', $cast -> umur) }}">
      </div>
      @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
          <label for="exampleFormControlTextarea1">Bio</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="bio">{{ old('bio', $cast -> bio ) }}</textarea>
        </div>
      @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror   
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    
@endsection