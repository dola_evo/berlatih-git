@extends('layouts.master')
@section('title')
    Isi Data Tabel Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm md-4"> tambah data</a>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $item -> nama }}</td>
                <td>{{ $item -> umur }}</td>
                <td>
                    <form action="/cast/{{ $item -> id }}" method="POST">
                        @csrf
                        <a href="/cast/{{ $item -> id }}" class="btn btn-info btn-sm">detail</a>
                        <a href="/cast/{{ $item -> id }}/edit" class="btn btn-warning btn-sm">update</a>
                        @method('delete')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
                
            </tr>
        @empty
            <tr>
                <td>Tidak ada data di table cast</td>
            </tr>
        @endforelse
     
     
    </tbody>
  </table>
@endsection 