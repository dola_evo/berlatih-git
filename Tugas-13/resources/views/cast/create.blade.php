@extends('layouts.master')
@section('title')
    halaman isi data
@endsection

@section('content')
  <form action="/cast" method="POST">
      @csrf
    <div class="form-group">
      <label for="exampleInputEmail1">Nama</label>
      <input type="text" class="form-control" name="nama" value="{{ old('nama') }}">
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
     
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Umur</label>
      <input type="text" class="form-control" name="umur" value="{{ old('umur') }}">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Bio</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="bio">{{ old('bio') }}</textarea>
      </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror   
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection