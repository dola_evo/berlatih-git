1. Membuat Database
CREATE DATABASE myshop;

2 Membuat Table di Dalam Database
CREATE TABLE users (
  id 	int AUTO_INCREMENT PRIMARY KEY,
  name 	varchar(255),
  email 	varchar(255),
  password 	varchar(255),
);
CREATE TABLE categories (
  id 	int AUTO_INCREMENT PRIMARY KEY,
  name 	varchar(255)
);
CREATE TABLE items (
  id 	int AUTO_INCREMENT PRIMARY KEY,
  name 	varchar(255),
  description 	varchar(255),
  price 	int,
  stock 	int,
  category_id 	int,
  FOREIGN KEY (category_id) REFERENCES categories(id)
);

3 Memasukkan Data pada Table

INSERT INTO users (name, email, password) VALUES ('John Doe','john@doe.com','john123'),('jane Doe', 'jane@doe.com', 'jenita123');
INSERT INTO categories (name) VALUES ('gadget'), ('cloth'),('men'), ('women'), ('branded');
INSERT INTO items (name, description, price, stock, category_id) VALUES ('Sumsang', 'b50', 'hape keren dari merek sumsang', 4000000,	100, 	1),('Uniklooh',	'baju keren dari brand ternama', 	500000,	50,	2),('IMHO Watch',	'jam tangan anak yang jujur banget', 2000000, 10,	1)

4 Mengambil Data dari Database

a. Mengambil data users

SELECT id, name, email FROM users;

b. Mengambil data items

1. SELECT * FROM items WHERE price > 1000000;
2. SELECT * FROM items LIKE '%watch%';

c. Menampilkan data items join dengan kategori

SELECT name.items , description.items, price.items, stock.items, category_id.items, name.categories FROM items LEFT JOIN on items.category_id = categories.id;

5 Mengubah Data dari Database

UPDATE items SET price = 2500000 WHERE name = 'sumsang b50';