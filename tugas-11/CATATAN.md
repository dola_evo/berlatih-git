# PENJELASAN RELASI TABLE

------------

**RELASI TABEL USER**
- one to one terhadap tabel profil
- one to many terhadap tabel kritik

**RELASI TABEL FILM**
- one to many terhadap tabel kritik
- one to many terhadap tabel peran

**TABEL CAST**
- one to many terhadap tabel peran

**RELASI TABEL GENRE**
- one to many terhadap tabel film

